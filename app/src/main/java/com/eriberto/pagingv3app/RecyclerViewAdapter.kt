package com.eriberto.pagingv3app

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eriberto.pagingv3app.network.CharacterData

class RecyclerViewAdapter :
    PagingDataAdapter<CharacterData, RecyclerViewAdapter.MyViewHolder>(DiffCalBack()) {

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        getItem(position)?.let { MyViewHolder(holder.view).bind(it) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val inflate =
            LayoutInflater.from(parent.context).inflate(R.layout.recyclerview_item, parent, false)
        return MyViewHolder(inflate)
    }

    inner class MyViewHolder(var view: View) : RecyclerView.ViewHolder(view) {
        private var tvName = view.findViewById<TextView>(R.id.tvName)
        private var tvSpecie = view.findViewById<TextView>(R.id.tvSpecie)
        private var imageView = view.findViewById<ImageView>(R.id.imageView)

        fun bind(item: CharacterData) {
            tvName.text = item.name
            tvSpecie.text = item.species

            Glide.with(imageView)
                .load(item.image)
                .circleCrop()
                .into(imageView)
        }
    }

    class DiffCalBack : DiffUtil.ItemCallback<CharacterData>() {
        override fun areItemsTheSame(oldItem: CharacterData, newItem: CharacterData): Boolean {
            return oldItem.name == newItem.name
        }

        override fun areContentsTheSame(oldItem: CharacterData, newItem: CharacterData): Boolean {
            return oldItem.name == newItem.name && oldItem.species == newItem.species
        }
    }
}