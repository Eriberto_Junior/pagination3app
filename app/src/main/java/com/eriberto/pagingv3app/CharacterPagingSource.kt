package com.eriberto.pagingv3app

import android.net.Uri
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.eriberto.pagingv3app.network.CharacterData
import com.eriberto.pagingv3app.network.RetroService

class CharacterPagingSource(val retroService: RetroService) : PagingSource<Int, CharacterData>() {
    override fun getRefreshKey(state: PagingState<Int, CharacterData>): Int? {
        return state.anchorPosition
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, CharacterData> {
        return try {

            val nextPage: Int = params.key ?: PRIMEIRA_PAGINA_INDEX
            val response = retroService.getDataFromApi(nextPage)

            var nextPageNumber: Int? = null

            if (response.info.next != null) {
                //extraindo queryParameter da URL nextPage
                val uri = Uri.parse(response.info.next)
                val nextPageQuery = uri.getQueryParameter("page")
                nextPageNumber = nextPageQuery?.toInt()
            }

            LoadResult.Page(data = response.results, prevKey = null, nextKey = nextPageNumber)

        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }

    companion object {
        private const val PRIMEIRA_PAGINA_INDEX = 1
    }
}