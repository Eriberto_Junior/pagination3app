package com.eriberto.pagingv3app.network

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface RetroService {

    @GET("character/")
    suspend fun getDataFromApi (@Query("page") page: Int): RickAndMortyList
}