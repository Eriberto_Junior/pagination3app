package com.eriberto.pagingv3app.network

data class RickAndMortyList(val info: Info, val results: List<CharacterData>)

data class Info(val count: Int?, val pages: Int?, val next: String?, val prev: String?)

data class CharacterData(val name: String?, val species: String?, val image: String?)
